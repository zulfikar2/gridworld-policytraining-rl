Zulfikar Yusufali 500618580

I have ran the program with multiple params as well as with the epsilon being reduced by 1/NUM_OF_EPISODES.
The run was with 500,000 originally, but monte carlo did not converge easily and it was reran with 1 million. I find it doesn't fully converge yet, but comes close.
Unfortunately It ran out of memory often on my local machine with one million episodes. I had to run it a couple times to have it go through once.
In that case I am showing the following results with 500,000 episodes as it is easier to run.
The steps were caclulated with each step being a loop until goal is reached. The avg states = TotalSteps / NUM_OF_EPISODES

The results show that though they converge differently and varies quite a bit at lower Episode numbers due to differences in algorithms as well
as the initial position being random.
Monte carlo when tested finishes the episodes, however the policy is not stable and most likely will take a long time to become so.
Q learning and SARSA converge to a stable policy the quickest and I have found SARSA to be the quickest in steps as well as average computation time.

Sample results (without show policy): 

Starting Monte-Carlo with 1000000 episodes and 0.1 epsilon.
Elapsed time (nanoTime) : 27204131820
Avg Num of Steps per episode : 66.599761

Starting Sarsa with 1000000 episodes 0.9 discount and alpha 0.1 and 0.1 epsilon
Elapsed time (nanoTime) : 28593093626
Avg Num of Steps per episode : 65.378172

Starting Q - LEARNING with 1000000 episodes 0.9 discount and alpha 0.1 and 0.1 epsilon
Elapsed time (nanoTime) : 31193883354
Avg Num of Steps per episode : 66.640688

Starting Monte-Carlo with 1000000 episodes and 0.2 epsilon.
Elapsed time (nanoTime) : 20362153955
Avg Num of Steps per episode : 47.588621

Starting Sarsa with 1000000 episodes 0.9 discount and alpha 0.2 and 0.2 epsilon
Elapsed time (nanoTime) : 25038034649
Avg Num of Steps per episode : 64.282143

Starting Q - LEARNING with 1000000 episodes 0.9 discount and alpha 0.2 and 0.2 epsilon
Elapsed time (nanoTime) : 29931903100
Avg Num of Steps per episode : 65.331864

Starting Monte-Carlo with 1000000 episodes and 0.05 epsilon.
Elapsed time (nanoTime) : 30632242909
Avg Num of Steps per episode : 54.142221

Starting Sarsa with 1000000 episodes 0.9 discount and alpha 0.05 and 0.05 epsilon
Elapsed time (nanoTime) : 29112512260
Avg Num of Steps per episode : 66.185276

Starting Q - LEARNING with 1000000 episodes 0.9 discount and alpha 0.05 and 0.05 epsilon
Elapsed time (nanoTime) : 30016536804
Avg Num of Steps per episode : 67.235891

