//Zulfikar Yusufali
//500618580
public class statePair {
  int x, y, a;
  statePair next;
  statePair(int x, int y) {this.x = x; this.y = y; next = null;}
  statePair(int x, int y, int a) {this.x = x; this.y = y; this.a = a; next = null;}
  int getX() { return x; }
  int getY() { return y; }
  int getA() { return a; }
  statePair getNext() { return next; }
  void setNext(statePair next) { this.next = next; }
  void setX(int x) { this.x = x; }
  void setY(int y) { this.y = y; }
  void setA(int a) { this.a = a; }
}