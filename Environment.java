//Zulfikar Yusufali
//500618580
import java.util.Random;
class Environment {
  double[][] gridWorld;
  double p1 = 1.0;
  double p2 = 0;
  //p1, p2, adjacent
  final int reward = -1, goal = 500;
  private final int POSSIBLE_ACTIONS = 3;
  private double[] ACTIONS, ACTIONS_WALL;
  Random r;
  
  Environment() {
    gridWorld = new double[10][10];
    r = new Random();
    ACTIONS = new double[POSSIBLE_ACTIONS];
    ACTIONS_WALL = new double[POSSIBLE_ACTIONS];
    ACTIONS[0] = p1; //move to action state
    ACTIONS[1] = p2; //same state
    ACTIONS[2] = (1.0-p1-p2)/2.0; //adj
    ACTIONS_WALL[0] = p1+p2; //same state
    ACTIONS_WALL[1] = (1.0-p1-p2)/2.0; //adj
  }
  
  Environment(double prob1, double prob2) {
    p1 = prob1;
    p2 = prob2;
     System.out.println("Environment Initialized with p1 : " + p1 + " and p2 : " + p2);
    gridWorld = new double[10][10];
    r = new Random();
    ACTIONS = new double[POSSIBLE_ACTIONS];
    ACTIONS_WALL = new double[POSSIBLE_ACTIONS];
    ACTIONS[0] = p1; //move to action state
    ACTIONS[1] = p2; //same state
    ACTIONS[2] = (1.0-p1-p2)/2.0; //adj
    ACTIONS_WALL[0] = p1+p2; //same state
    ACTIONS_WALL[1] = (1.0-p1-p2)/2.0; //adj
  }
  
  double[][] getGridWorld() { return gridWorld; }
  void setGridWorld(int x, int y, float val) { gridWorld[x][y] = val; }
  
  boolean checkWall(int x, int y, int newX, int newY) {
    //adjust for position values and cell coords
    int posX = x+1; 
    int posY = y+1; 
    int newPosX = newX+1; 
    int newPosY = newY+1;
    //System.out.println("x = " + posX + " y = " + posY + " newX = " + newPosX + " newY = " + newPosY);
    //hitting first wall
    if(posX == 3 && newX == 4 && posY <= 8) {
      return true;
    }
    //hitting second wall
    else if(posX == 6 && newPosX == 7 && posY >= 3) {
      return true;
    }
    //hitting Left wall
    else if(posX <= 0 || newPosX <= 0) {
      return true;
    }
    //hitting Top wall
    else if(posY >= 10 || newPosY >= 10) {
      return true;
    }
    //hitting Right wall
    else if(posX >= 10 || newPosX >= 10) {
      return true;
    }
    //hitting Bottom wall
    else if(posY <= 0 || newPosY <= 0) {
      return true;
    }
    else {
      return false;
    }
  }
  
  //actions = {UP, DOWN, RIGHT, LEFT}
  //return amount to change in each axis
  //example : {-1,0} means go left on x axis
  int[] move(int action) {
    int[] cords = new int[2];
    switch(action) {
      //UP, DOWN, RIGHT, LEFT
      case 0:
      case 1:
      case 2:
      case 3:
        double probability = Math.random();
        double culmProbability = 0;
        for(int i = 0; i < POSSIBLE_ACTIONS; i++) {
          culmProbability += ACTIONS[i];
          if(culmProbability >= probability) {
            return getCords(i, action);
          }
        }
        return new int[2];
        
      default:
        System.out.println("ERROR?? Action : " + action);
        System.exit(0);
    }
    return cords;
  }
  
  int[] getCords(int i, int action) {
    int[] cords = new int[2];
    switch(action) {
      //UP
      case 0:
        switch(i) {
        //Do action
        case 0:
          return new int[] {0,1};
        //Do nothing
        case 1:
          return new int[] {0,0};
        //Go to Adj
        case 2:
          int randAdj = r.nextInt(2);
          if(randAdj == 0)
            return new int[] {1,0};
          else
            return new int[] {-1,0};
      }
        break;
        //DOWN
      case 1:
        switch(i) {
        case 0:
          return new int[] {0,-1};
        case 1:
          return new int[] {0,0};
        case 2:
          int randAdj = r.nextInt(2);
          if(randAdj == 0)
            return new int[] {1,0};
          else
            return new int[] {-1,0};
      }
        break;
        //RIGHT
      case 2:
        switch(i) {
        case 0:
          return new int[] {1,0};
        case 1:
          return new int[] {0,0};
        case 2:
          int randAdj = r.nextInt(2);
          if(randAdj == 0)
            return new int[] {1,0};
          else
            return new int[] {-1,0};
      }
        break;
        //LEFT
      case 3:
        switch(i) {
        case 0:
          return new int[] {-1,0};
        case 1:
          return new int[] {0,0};
        case 2:
          int randAdj = r.nextInt(2);
          if(randAdj == 0)
            return new int[] {1,0};
          else
            return new int[] {-1,0};
      }
        break;
        //ERROR
      default:
        System.out.println("ERROR! " + i + " Action : " + action);
        System.exit(0);
    }
    return new int[2];
  }
  
  int[] moveWall(int x, int y, int newX, int newY) {
    double probability = Math.random();
    double culmProbability = 0;
    int[] cords = new int[2];
    int action = -1;
    for(int i = 0; i < POSSIBLE_ACTIONS-1; i++) {
      culmProbability += ACTIONS_WALL[i];
      if(probability <= culmProbability) {
        action = i;
      }
    }
    switch(action) {
      //Do nothing
      case 0:
        cords = new int[] {0,0};
        break;
      //Move Adj
      case 1:
        int randAdj = r.nextInt(2);
        if(randAdj == 0)
          cords = new int[] {1,0};
        else
          cords = new int[] {-1,0};
        break;
      default:
        System.out.println("ERROR! " + " Action : " + action);
        System.exit(0);
    }
    if(x+cords[0] >= 10)
      cords[0] = 0;
    if(y+cords[1] >= 10)
      cords[1] = 0;
    if(x+cords[0] < 0)
      cords[0] = 0;
    if(y+cords[1] < 0)
      cords[1] = 0;
    
    return cords;
  }
  
  boolean terminatingState(int x, int y){
    //adjust for array value to cell value
    int posX = x+1; int posY = y+1;
    if(posX >= 10 && posY >= 10)
      return true;
    else
      return false;
  }
  
  int getReward(int x, int y) {
    if(terminatingState(x,y))
      return goal;
    else
      return reward;
  }
  
  int randomWithRange(int min, int max)
  {
    int range = (max - min) + 1;     
    return (int)(Math.random() * range) + min;
  }
}