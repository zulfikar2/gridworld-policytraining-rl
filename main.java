//Zulfikar Yusufali
//500618580
/*implement and compare experimentally 3 well-known learning algorithms:
Monte-Carlo on-policy control using ?-greedy policies, Q-learning and Sarsa.*/
import java.util.*;
public class main {
  public static void main(String [] args) {
    int MAX_EPISODES = 1000000;
    double EPSILON = 0.1;
    double DISCOUNT = 0.9;
    double ALPHA = 0.1;
    mc monteCarlo;
    sarsa sarsa;
    q q;
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter p1 : ");
    double p1 = scan.nextDouble();
    System.out.println("Enter p2 : ");
    double p2 = scan.nextDouble();
    Environment env = new Environment(p1,p2);
    
    monteCarlo = new mc(MAX_EPISODES, EPSILON, env);
    sarsa = new sarsa(MAX_EPISODES, EPSILON, DISCOUNT, ALPHA, env);
    q = new q(MAX_EPISODES, EPSILON, DISCOUNT, ALPHA, env);
  }
}