# Gridworld Optimal Policy finder for an unknown MDP

A reinforcement learning algorithm is an algorithm that tries to construct an optimal
policy for an unknown MDP (i.e., in an environment where transition probabilities and expected rewards
are unknown). The algorithm is given access to the unknown MDP via the following protocol. At each
time step t, the algorithm is told the current state s of the MDP and the set of actions A(s) ⊆ A that are
executable in that state. The algorithm chooses an action a ∈ A(s), and the agent executes this action,
which causes the unknown MDP to move to state s ′ . Also, in response to a, an environment returns a
real-valued immediate reward signal r.

![](Explanation1.png)
![](Explanation2.png)