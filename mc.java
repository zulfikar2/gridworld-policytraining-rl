//Zulfikar Yusufali
//500618580
import java.util.*;
import java.util.Random;
/*Monte-Carlo on-policy control using ?-greedy policies (follow the posted handout implementing
 Blackjack), assuming the agent starts each episode in the randomly chosen initial state
 with a random action.*/
public class mc {
  Environment env;
  private final boolean SHOW_EPISODES = false;
  private int MAX_EPISODES;
  private double EPSILON;
  private final int NUMSTATES_X = 10;
  private final int NUMSTATES_Y = 10;
  private final int ACTIONS = 4;
  private final int UP = 0, DOWN = 1, RIGHT = 2, LEFT = 3;
  //Q(s,a) = action values of state s with action a
  private double[][][] Q = new double[NUMSTATES_X][NUMSTATES_Y][ACTIONS];
  //P(s,a) = probability of taking action a at state s
  private double[][][] P = new double[NUMSTATES_X][NUMSTATES_Y][ACTIONS];
  //W(s,a) = num of times statePair(s,a) encountered
  private int[][][] W = new int[NUMSTATES_X][NUMSTATES_Y][ACTIONS];
  private int steps = 0;
  long startTime, stopTime, elapsedTime;
  mc(int MAX_EPISODES, double EPSILON, Environment envir) {
    System.out.println("Starting Monte-Carlo with " + MAX_EPISODES + " episodes and " + EPSILON + " epsilon.");
    startTime = System.nanoTime();
    env = envir;
    this.MAX_EPISODES = MAX_EPISODES;
    this.EPSILON = EPSILON;
    initialize();
    statePair appearedPairs;
    statePair newPair;
    int positionX = 0;
    int positionY = 0;
    int rewards = 0;
    int[] stepTrack = new int[MAX_EPISODES];
    
    for(int episodeNum = 0; episodeNum < MAX_EPISODES; episodeNum++) {
      rewards = 0;
      appearedPairs = null;
      steps = 0;
      positionX = (int)(Math.floor(Math.random()*10)); //0-9 range
      positionY = (int)(Math.floor(Math.random()*10));
      if(SHOW_EPISODES) { System.out.println("New Episode " + (episodeNum+1) + " X=" + positionX + " Y=" + positionY); }
      //else if(episodeNum%10000 == 0) {System.out.println("New Episode " + (episodeNum+1) + " X=" + positionX + " Y=" + positionY);}
      while(!env.terminatingState(positionX, positionY)) {
        steps++;
        int action = selectAction(positionX, positionY);
        newPair = new statePair(positionX, positionY, action);
        newPair.setNext(appearedPairs);
        appearedPairs = newPair;
        
        int[] posChange = env.move(action);
        if(env.checkWall(positionX, positionY, posChange[0], posChange[1])) {
          //System.out.println("WALL!");
          posChange = env.moveWall(positionX, positionY, posChange[0], posChange[1]);
        }
        /*if(positionX+posChange[0] >= 10)
          System.out.println("ERROR : " + positionX + " " + posChange[0]);
        if(positionY+posChange[1] >= 10)
          System.out.println("ERROR : " + positionY + " " + posChange[0]);*/
        positionX += posChange[0];
        positionY += posChange[1];
        //System.out.println("newX = " + positionX + " newY = " + positionY);
        rewards += env.getReward(positionX, positionY);
      }
      //update Q stuff
      while(appearedPairs != null) {
        int x = appearedPairs.getX();
        int y = appearedPairs.getY();
        int a = appearedPairs.getA();
        Q[x][y][a] = Q[x][y][a] + (double)(rewards - Q[x][y][a])/(W[x][y][a] + 1);
        //System.out.println(Q[x][y][a]);
        W[x][y][a] += 1;
        appearedPairs = appearedPairs.getNext();
      }
      //update policy using new Q stuff
      for(int x = 0; x < NUMSTATES_X; x++) {
        for(int y = 0; y < NUMSTATES_Y; y++) {
          updatePolicy(x,y);
        }
      }
      this.EPSILON -= 1/MAX_EPISODES;
      stepTrack[episodeNum] = steps;
    }
    stopTime = System.nanoTime();
    elapsedTime = stopTime - startTime;
    System.out.println("Elapsed time (nanoTime) : " + elapsedTime);
    for(int x = 0; x < MAX_EPISODES; x++)
      steps += stepTrack[x];
    double avgSteps = (double)steps/MAX_EPISODES;
    System.out.println("Avg Num of Steps per episode : " + avgSteps);
    showPolicy();
  }
  
  void showPolicy() {
    System.out.println("-------------- POLICY FOR MONTE CARLO --------------");
    for(int x = 0; x < NUMSTATES_X; x++) {
      for(int y = 0; y < NUMSTATES_Y; y++) {
        //ignore Goal State
        if(x == 9 && y == 9)
          break;
        int action = maxAction(x,y);
        String actionS = " - ";
        switch(action) {
          case 0:
            actionS = "UP";
            break;
          case 1:
            actionS = "DOWN";
            break;
          case 2:
            actionS = "RIGHT";
            break;
          case 3:
            actionS = "LEFT";
            break;
        }
        System.out.println("State x=" + x + " y=" + y + "  action = " + actionS);
      }
    }
  }
  
  void updatePolicy(int x, int y) {
    int action = maxActionQ(x,y);
    switch(action) {
      case 0:
        P[x][y][UP] = 1.0 - EPSILON + EPSILON/ACTIONS;
        P[x][y][DOWN] = EPSILON/ACTIONS;
        P[x][y][RIGHT] = EPSILON/ACTIONS;
        P[x][y][LEFT] = EPSILON/ACTIONS;
        break;
      case 1:
        P[x][y][UP] = EPSILON/ACTIONS;
        P[x][y][DOWN] = 1.0 - EPSILON + EPSILON/ACTIONS;
        P[x][y][RIGHT] = EPSILON/ACTIONS;
        P[x][y][LEFT] = EPSILON/ACTIONS;
        break;
      case 2:
        P[x][y][UP] = EPSILON/ACTIONS;
        P[x][y][DOWN] = EPSILON/ACTIONS;
        P[x][y][RIGHT] = 1.0 - EPSILON + EPSILON/ACTIONS;
        P[x][y][LEFT] = EPSILON/ACTIONS;
        break;
      case 3:
        P[x][y][UP] = EPSILON/ACTIONS;
        P[x][y][DOWN] = EPSILON/ACTIONS;
        P[x][y][RIGHT] = EPSILON/ACTIONS;
        P[x][y][LEFT] = 1.0 - EPSILON + EPSILON/ACTIONS;
        break;
      default:
        System.out.println("Update ERROR : " + x + " " + y);
        System.exit(0);
    }
  }
  
  void initialize() {
    //init Q & W
    for(int x = 0; x < NUMSTATES_X; x++) {
      for(int y = 0; y < NUMSTATES_Y; y++) {
        for(int a = 0; a < ACTIONS; a++) {
          Q[x][y][a] = 0;
          W[x][y][a] = 0;
        }
      }
    }
    
    //init arbitrary policy : UP
    for(int x = 0; x < NUMSTATES_X; x++) {
      for(int y = 0; y < NUMSTATES_Y; y++) {
        P[x][y][UP] = 1.0 - EPSILON + EPSILON/ACTIONS;
        P[x][y][DOWN] = EPSILON/ACTIONS;
        P[x][y][RIGHT] = EPSILON/ACTIONS;
        P[x][y][LEFT] = EPSILON/ACTIONS;
      }
    }
  }
  
  int selectAction(int x, int y) {
    double p = Math.random();
    if(x >= 10 || y >= 10) {
      System.out.println("ERROR Select Action : x= " + x + " y=" + y);
      System.exit(0);
    }
    if(p <= (1.0 - EPSILON + EPSILON/ACTIONS)) {
      return maxAction(x,y);
    }
    else {
      double cumulativeProbability = 0.0;
      for (int a = 0; a < ACTIONS; a++) {
        cumulativeProbability += P[x][y][a];
        if (cumulativeProbability >= p) {
          return a;
        }
      }
    }
    return UP;
  }
  
  int maxAction(int x, int y) {
    double max = 0;
    int maxA = 0;
    for(int a = 0; a < ACTIONS; a++) {
      if(P[x][y][a] > max) {
        max = P[x][y][a];
        maxA = a;
      }
    }
    return maxA;
  }
  
  int maxActionQ(int x, int y) {
    double max = 0;
    int maxA = 0;
    for(int a = 0; a < ACTIONS; a++) {
      if(Q[x][y][a] > max) {
        max = Q[x][y][a];
        maxA = a;
      }
    }
    return maxA;
  }
}