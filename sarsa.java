//Zulfikar Yusufali
//500618580
import java.util.*;
import java.util.Random;
/*SARSA : An on-policy TD control algorithm*/
public class sarsa {
  Environment env;
  private final boolean SHOW_EPISODES = false;
  private int MAX_EPISODES;
  private double EPSILON;
  private double DISCOUNT;
  private double ALPHA;
  private final int NUMSTATES_X = 10;
  private final int NUMSTATES_Y = 10;
  private final int ACTIONS = 4;
  private final int UP = 0, DOWN = 1, RIGHT = 2, LEFT = 3;
  //Q(s,a) = action values of state s with action a
  private double[][][] Q = new double[NUMSTATES_X][NUMSTATES_Y][ACTIONS];
  //P(s,a) = probability of taking action a at state s
  private double[][][] P = new double[NUMSTATES_X][NUMSTATES_Y][ACTIONS];
  private int steps = 0;
  long startTime, stopTime, elapsedTime;
  
  sarsa(int episodes, double epsilon, double discount, double alpha, Environment envir) {
    this.DISCOUNT = discount;
    this.ALPHA = alpha;
    this.EPSILON = epsilon;
    this.MAX_EPISODES = episodes;
    System.out.println("Starting Sarsa with " + MAX_EPISODES + " episodes " + DISCOUNT + " discount and alpha " + ALPHA + "and " + epsilon + " epsilon");
    startTime = System.nanoTime();
    env = envir;
    initialize();
    int rewards = 0;
    int[] stepTrack = new int[MAX_EPISODES];
    int positionX = 0;
    int positionY = 0;
    
    for(int episodeNum = 0; episodeNum < MAX_EPISODES; episodeNum++) {
      //initialize S
      positionX = (int)(Math.floor(Math.random()*10)); //0-9 range
      positionY = (int)(Math.floor(Math.random()*10));
      rewards = 0;
      steps = 0;
      //choose A from S
      int action = selectAction(positionX, positionY);
      
      if(SHOW_EPISODES) { System.out.println("New Episode " + (episodeNum+1) + " X=" + positionX + " Y=" + positionY);}
      
//Repeat for each Step of episode
      while(!env.terminatingState(positionX, positionY)) {
        steps++;
        //take A
        int[] posChange = env.move(action);
        if(env.checkWall(positionX, positionY, posChange[0], posChange[1])) {
          posChange = env.moveWall(positionX, positionY, posChange[0], posChange[1]);
        }

        //observe R from S
        rewards += env.getReward(positionX, positionY);
        //S'
        int positionXP = positionX + posChange[0];
        int positionYP = positionY + posChange[1];
        
        //choose A' from S'
        int actionP =  selectAction(positionXP, positionYP);
        
        //Q update
        updateQ(positionX, positionY, positionXP, positionYP, action, actionP, rewards);

        //S = S'
        positionX = positionXP;
        positionY = positionYP;
        //A = A'
        action = actionP;
      }
      this.EPSILON -= 1/MAX_EPISODES;
      stepTrack[episodeNum] = steps;
    }
    stopTime = System.nanoTime();
    elapsedTime = stopTime - startTime;
    System.out.println("Elapsed time (nanoTime) : " + elapsedTime);
    for(int x = 0; x < MAX_EPISODES; x++)
      steps += stepTrack[x];
    double avgSteps = (double)steps/MAX_EPISODES;
    System.out.println("Avg Num of Steps per episode : "  + avgSteps);
    showPolicy();
  }
  
  void updateQ(int x, int y, int xp, int yp, int a, int ap ,int rewards) {
    Q[x][y][a] = Q[x][y][a] + ALPHA * (rewards + (DISCOUNT * Q[xp][yp][ap]) - Q[x][y][a]);
    //System.out.println(Q[x][y][a]);
  }
  
  void initialize() {
    //init Q
    for(int x = 0; x < NUMSTATES_X; x++) {
      for(int y = 0; y < NUMSTATES_Y; y++) {
        for(int a = 0; a < ACTIONS; a++) {
          if(env.terminatingState(x,y))
            Q[x][y][a] = 0;
          else
            Q[x][y][a] = 5;
        }
      }
    }
  }
  
  int selectAction(int x, int y) {
    double p = Math.random();
    if(x >= 10 || y >= 10 || x < 0 || y < 0) {
      System.out.println("ERROR Select Action : x= " + x + " y=" + y);
      System.exit(0);
    }
    if(p >= (1.0 - EPSILON)) {
      return maxActionQ(x,y);
    }
    else {
      p = Math.floor(Math.random()*4); //0-3
      return (int)p;
    }
  }
  
  int maxActionQ(int x, int y) {
    double max = -5000;
    int maxA = 0;
    for(int a = 0; a < ACTIONS; a++) {
      if(Q[x][y][a] > max) {
        max = Q[x][y][a];
        maxA = a;
      }
    }
    return maxA;
  }
  
  void showPolicy() {
    System.out.println("-------------- POLICY FOR SARSA --------------");
    for(int x = 0; x < NUMSTATES_X; x++) {
      for(int y = 0; y < NUMSTATES_Y; y++) {
        //ignore Goal State
        if(x == 9 && y == 9)
          break;
        int action = maxActionQ(x,y);
        String actionS = " - ";
        switch(action) {
          case 0:
            actionS = "UP";
            break;
          case 1:
            actionS = "DOWN";
            break;
          case 2:
            actionS = "RIGHT";
            break;
          case 3:
            actionS = "LEFT";
            break;
        }
        System.out.println("State x=" + x + " y=" + y + "  action = " + actionS);
      }
    }
  }
}
